<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateFAQRequest;
use App\Repositories\FAQRepository;
use Illuminate\Support\Facades\App;
use Laracasts\Flash\Flash;

class ContactController extends Controller
{
    public function index()
    {
        return view('web.contact');
    }

    /**
     * Store a newly created FAQ in storage.
     *
     * @param  CreateFAQRequest  $request
     *
     * @return Response
     */
    public function store(CreateFAQRequest $request)
    {
        $fAQRepository = App::make(FAQRepository::class);
        $input = $request->all();

        $faq = $fAQRepository->create($input);

        Flash::success('Deatils saved successfully.');

        return redirect()->back();
    }
}
