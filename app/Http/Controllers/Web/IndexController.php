<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Model\BookCategory;
use App\Model\Category;
use App\Model\HomeSlider;

class IndexController extends Controller
{
    //

    public function index()
    {
        $data['homeSliders'] = HomeSlider::all();
        $data['categories'] = Category::with('count_sub_category')
            ->orderBy('serial', 'ASC')->whereNull('parent_id')->get();
        $data['bookCategories'] = BookCategory::has('books')->latest()->take(5)->get();
        $data['settings'] = allsetting();

        return view('web.index', $data);
    }
}
