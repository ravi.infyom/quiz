<div class="container  mt-5">
    <div class="col-md-12">
        <h2 class="text-center section_title">
            All Book Category & Books
        </h2>
    </div>
    <div class="col-md-12 mt-4">
        @foreach($bookCategories as $category)
            <div class="d-flex my-5">
                <h4 class="font-weight-bold book_category_name">
                    {{ $category->name }}
                </h4>
                <a href="{{ route('web.showBooks') }}" class="ml-5 font-weight-bold">
                    View All <i class="fas fa-angle-double-right"></i>
                </a>
            </div>

            <div class="row team_row">
                @forelse($category->books as $book)
                    <div class="col-md-3 mb-3">
                        <div class="feature card-hover-border px-0 text-center trans_400 border rounded">
                            <div class="feature_icon">
                                <img src="{{ asset('web_assets/images/marker.png') }}"
                                     alt=""></div>
                            <h3 class="feature_title">{{ $book->name }}</h3>
                            <a href="javascript:void(0)" class="btn btn-primary  mt-4">View Book</a>
                        </div>
                    </div>
                @empty
                    <h4>No Sub Category Found </h4>
                @endforelse
            </div>
        @endforeach
    </div>
</div>
