<?php

use App\Model\AdminSetting;
use Illuminate\Database\Seeder;

class AdminSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminSetting::create(['slug' => 'app_title', 'value' => 'Quiz']);
        AdminSetting::create(['slug' => 'logo', 'value' => '']);
        AdminSetting::create(['slug' => 'login_logo', 'value' => '']);
        AdminSetting::create(['slug' => 'favicon', 'value' => '']);
        AdminSetting::create(['slug' => 'copyright_text', 'value' => 'Copyright@2018']);
        //General Settings
        AdminSetting::create(['slug' => 'lang', 'value' => 'en']);
        AdminSetting::create(['slug' => 'company_name', 'value' => 'New Company']);
        AdminSetting::create(['slug' => 'primary_email', 'value' => 'info@email.com']);
        AdminSetting::create(['slug' => 'user_registration', 'value' => 1]);
        AdminSetting::create(['slug' => 'is_authenticated', 'value' => LICENSE_VERIFIED]);
        AdminSetting::create(['slug' => 'app_phone', 'value' => '123456798']);
        AdminSetting::create(['slug' => 'landing_footer_address', 'value' => '123456798']);
        AdminSetting::create(['slug'  => 'landing_footer_des',
                              'value' => 'Lorem ipsum dolor sit ametium, consectetur adipiscing elit.',
        ]);
        AdminSetting::create(['slug' => 'facebook_url', 'value' => 'https::/www.facebook.com']);
        AdminSetting::create(['slug' => 'twitter_url', 'value' => 'https::/www.facebook.com']);
        AdminSetting::create(['slug' => 'instagram_url', 'value' => 'https::/www.facebook.com']);
        AdminSetting::create(['slug' => 'google_url', 'value' => 'https::/www.facebook.com']);
        AdminSetting::create(['slug' => 'landing_contact_title', 'value' => 'Conatct US']);
        AdminSetting::create(['slug'  => 'landing_contact_des',
                              'value' => 'Lorem ipsum dolor sit ametium, consectetur adipiscing elit.',
        ]);
        AdminSetting::create(['slug' => 'landing_feature_title', 'value' => 'Welcome To Website']);
        AdminSetting::create(['slug'  => 'landing_feature_des',
                              'value' => 'Lorem ipsum dolor sit ametium, consectetur adipiscing elit.',
        ]);
    }
}
